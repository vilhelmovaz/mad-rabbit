﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using mad_rabbit;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using KeyEventHandler = System.Windows.Input.KeyEventHandler;

namespace mad_rabbit_UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            GameTimer = new Stopwatch();
            InitializeComponent();
        }

        private Stopwatch GameTimer { get; }

        private TimeSpan PreviousGameTick;

        private Dictionary<string, Texture> TexturesDictionary = new Dictionary<string, Texture>();

        private float EllapsedSecondsSinceLastTick => (float) (GameTimer.Elapsed - PreviousGameTick).TotalSeconds;

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            Menu.Visibility = Visibility.Hidden;
            paintCanvas.Visibility = Visibility.Visible;
            GameStatus.Visibility = Visibility.Visible;
            var game = Game.Instance;
            game.Init(false);
            CompositionTarget.Rendering += new EventHandler(timer_Tick);
            this.KeyDown += new KeyEventHandler(OnButtonKeyDown);
            this.KeyUp += new KeyEventHandler(OnButtonKeyUp);
            GameTimer.Start();
        }
        
        

        private void timer_Tick(object sender, EventArgs e)
        {
            paintCanvas.Children.Clear();
            
            Game.Instance.OnUpdate(EllapsedSecondsSinceLastTick);
            PreviousGameTick = GameTimer.Elapsed;
            var sprites = Game.Instance.DrawingManager.GetSpritesForDrawing();
            
            var offset = Game.Instance.Map.Offset;
            Score.Text = "Score: " + Game.Instance.Score;
            Lives.Text = "Lives: " + Game.Instance.Lives;
            Level.Text = "Level " + (Game.Instance.Level.Equals(100) ? "Custom" : Game.Instance.Level.ToString());
            foreach (var sprite in sprites)
            {
                if (sprite.Position.X - offset >= this.Width  || sprite.Position.X + Constants.BlockSize - offset < 0)
                {
                    continue;
                }
                
                if (sprite?.Texture == null)
                {
                    continue;
                }

                var i = new Image();
                TexturesDictionary.TryGetValue(sprite.Texture, out var texture);

                
                if (texture == null)
                {
                    // nacteni obrazku z resources
                    texture = new Texture();
                    var src = new BitmapImage();
                    src.BeginInit();
                    src.UriSource = new Uri("Resources/" + sprite.Texture, UriKind.Relative);
                    src.CacheOption = BitmapCacheOption.OnLoad;
                    texture.Image = src;
                    src.EndInit();
                    var test = sprite.Texture.Substring(sprite.Texture.IndexOf('.') + 1, 1);
                    int.TryParse(sprite.Texture.Substring(sprite.Texture.IndexOf('.') + 1, 1), out var maxFrames);
                    texture.MaxFrames = Math.Max(maxFrames, 1);
                    TexturesDictionary.Add(sprite.Texture, texture);
                }

                i.Source = texture.Image;
                i.Stretch = Stretch.Uniform;
                if (texture.MaxFrames > 0)
                {
                }
               
                var frameWidth = texture.Image.Width / texture.MaxFrames; // sirka jednoho framu obrazku animace
                var frame = (int) (texture.MaxFrames * sprite.AnimationFrame); // index animace
                i.Clip = new RectangleGeometry(new Rect(frameWidth * frame, 0, frameWidth, texture.Image.Height)); // oriznuti textury s jednim framem animace

                Canvas.SetTop(i, sprite.Position.Y);
                if (!sprite.Flipped)
                {
                    Canvas.SetLeft(i, sprite.Position.X - frameWidth * frame - offset);
                }
                else
                {
                    Canvas.SetLeft(i, sprite.Position.X + frameWidth * (frame + 1) - offset);
                }
                
                i.RenderTransform = new ScaleTransform(sprite.Flipped ? -1 : 1, 1);

                paintCanvas.Children.Add(i);
                if (Game.Instance.NextLevel)
                {
                    TotalScore.Text = "Total Score: " + Game.Instance.Score + " / " + Game.Instance.MaxScore + " ( " +
                                      (Game.Instance.Score / Game.Instance.MaxScore) * 100 + "% ) ";
                    ButtonCanvas.Visibility = Visibility.Visible;
                    if (Game.Instance.Level >= Constants.MaxLevel)
                    {
                        LoadLevel.Visibility = Visibility.Hidden;
                    }
                    GameTimer.Stop();
                }

                if (Game.Instance.Gameover)
                {
                    ButtonCanvas.Visibility = Visibility.Visible;
                    TextBlock.Text = "Game Over";
                    LoadLevel.Visibility = Visibility.Hidden;
                    GameTimer.Stop();
                    
                }
            }
        }
        
        private void loadLevel_Click(object sender, EventArgs e)
        {
            ButtonCanvas.Visibility = Visibility.Hidden;
            Game.Instance.Init(false);
            GameTimer.Start();
        }

        private static void OnButtonKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Space:
                    Game.Instance.InputManager.Up = true;
                    break;
                case Key.Left:
                case Key.A:
                    Game.Instance.InputManager.Left = true;
                    break;
                case Key.Right:
                case Key.D:
                    Game.Instance.InputManager.Right = true;
                    break;
                case Key.LeftCtrl:
                case Key.RightCtrl:
                    Game.Instance.InputManager.Killing = true;
                    break;
            }
        }

        private static void OnButtonKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Space:
                    Game.Instance.InputManager.Up = false;
                    break;
                case Key.Left:
                case Key.A:
                    Game.Instance.InputManager.Left = false;
                    break;
                case Key.Right:
                case Key.D:
                    Game.Instance.InputManager.Right = false;
                    break;
                case Key.LeftCtrl:
                case Key.RightCtrl:
                    Game.Instance.InputManager.Killing = false;
                    break;
            }
        }

        private void Menu_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonCanvas.Visibility = Visibility.Hidden;
            paintCanvas.Visibility = Visibility.Hidden;
            GameStatus.Visibility = Visibility.Hidden;
            Menu.Visibility = Visibility.Visible;
            Game.Instance.clear();
            Game.Instance.Gameover = false;
            Game.Instance.NextLevel = false;
            Game.Instance.Level = 0;
        }

        private void Custom_OnClick(object sender, RoutedEventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //Get the path of specified file
                    Game.Instance.Customfile = openFileDialog.FileName;
                    Menu.Visibility = Visibility.Hidden;
                    paintCanvas.Visibility = Visibility.Visible;
                    GameStatus.Visibility = Visibility.Visible;
                    Game.Instance.Init(true);
                    CompositionTarget.Rendering += new EventHandler(timer_Tick);
                    this.KeyDown += new KeyEventHandler(OnButtonKeyDown);
                    this.KeyUp += new KeyEventHandler(OnButtonKeyUp);
                    GameTimer.Start();
                }
            }
        }
    }
}