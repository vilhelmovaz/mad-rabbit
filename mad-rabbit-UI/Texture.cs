﻿using System.Windows.Media.Imaging;

namespace mad_rabbit
{
    class Texture
    {
        public BitmapImage Image { get; set; }
        public int MaxFrames { get; set; }
    }
}