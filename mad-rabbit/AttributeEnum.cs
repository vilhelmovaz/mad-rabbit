namespace mad_rabbit
{
    public enum AttributeEnum
    {
        START_POSITION,
        END_POSITION,
        VALUE,
        VELOCITY,
        ANIMATION_SPEED,
        HEIGHT,
        WIDTH,
        SIZE,
        TIMEOUT,
        ON_GROUND,
        ALIVE
    }
}