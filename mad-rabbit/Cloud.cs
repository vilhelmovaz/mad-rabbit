using System.Numerics;
using mad_rabbit.Components;

namespace mad_rabbit
{
    public class Cloud : Entity
    {
        public Cloud(TypeEnum type, Vector2 startPosition)
        {
            Attributes.Add(AttributeEnum.START_POSITION, startPosition);
            Attributes.Add(AttributeEnum.VELOCITY, new Vector2(Constants.CloudSpeed, 0));
            Attributes.Add(AttributeEnum.WIDTH, 124);
            Type = type;
            Components.Add(new DrawComponent());
            Components.Add(new CloudComponent());
        }
    }
}