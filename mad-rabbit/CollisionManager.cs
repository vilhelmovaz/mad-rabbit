using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Windows;
using mad_rabbit.Messages;

namespace mad_rabbit
{
    public class CollisionManager
    {
        private HashSet<Entity> Entities { get; set; } = new HashSet<Entity>();

        private static CollisionManager _instance;
        private static readonly object InstanceLock = new object();

        public static CollisionManager Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                lock (InstanceLock)
                {
                    _instance = new CollisionManager();
                }

                return _instance;
            }
        }

        public void Update(float deltaT)
        {
            foreach (var entity in Entities)
            {
                switch (entity.Type)
                {
                    case TypeEnum.RABBIT:
                        entity.Attributes[AttributeEnum.ON_GROUND] = false;
                        collisionWithGround(entity, deltaT);
                        var rabbitWidth = (int) entity.Attributes[AttributeEnum.WIDTH];
                        var rabbitHeight = (int) entity.Attributes[AttributeEnum.HEIGHT];
                        foreach (var entity2 in Entities)
                        {
                            if (!entity.Equals(entity2))
                            {
                                if (entity2 is Item)
                                {
                                    var rabbitX = entity.Sprite.Position.X +
                                                  ((Vector2) entity.Attributes[AttributeEnum.VELOCITY]).X * deltaT;
                                    var rabbitY = entity.Sprite.Position.Y;
                                    var entity2Size = (int) entity2.Attributes[AttributeEnum.SIZE];
                                    if (isInCollision(rabbitX, rabbitY, rabbitHeight, rabbitWidth,
                                        entity2.Sprite.Position.X, entity2.Sprite.Position.Y, entity2Size,
                                        entity2Size))
                                    {
                                        MessageManager.Instance.BroadcastMessage(new CollisionWithItemMessage(entity2),
                                            this);
                                        break;
                                    }
                                }

                                if (entity2 is Monster && (bool)entity2.Attributes[AttributeEnum.ALIVE])
                                {
                                    var rabbitX = entity.Sprite.Position.X +
                                                  ((Vector2) entity.Attributes[AttributeEnum.VELOCITY]).X * deltaT;
                                    var rabbitY = entity.Sprite.Position.Y;

                                    if (isInCollision(rabbitX, rabbitY, rabbitHeight, rabbitWidth,
                                        entity2.Sprite.Position.X, entity2.Sprite.Position.Y, Constants.BlockSize,
                                        Constants.BlockSize))
                                    {
                                        MessageManager.Instance.BroadcastMessage(
                                            new CollisionWithMonsterMessage(entity),
                                            this);
                                        break;
                                    }
                                }
                            }
                        }

                        break;
                    case TypeEnum.MISSILE:
                        var positionX = entity.Sprite.Position.X;
                        var positionY = entity.Sprite.Position.Y;
                        foreach (var entity2 in Entities)
                        {
                            if (!entity.Equals(entity2))
                            {
                                if (entity2 is Monster && (bool)entity2.Attributes[AttributeEnum.ALIVE])
                                {
                                    if (isInCollision(positionX, positionY, Constants.MissileSize, Constants.MissileSize,
                                        entity2.Sprite.Position.X, entity2.Sprite.Position.Y, Constants.BlockSize,
                                        Constants.BlockSize))
                                    {
                                        MessageManager.Instance.BroadcastMessage(
                                            new CollisionWithMissileMessage(entity2, entity),
                                            this);
                                    }
                                }
                            }
                        }

                        foreach (var sprite in Game.Instance.Map.GetSprites())
                        {
                            if (isInCollision(positionX, positionY, Constants.MissileSize, Constants.MissileSize,
                                sprite.Position.X, sprite.Position.Y, Constants.BlockSize,Constants.BlockSize))
                            {
                                MessageManager.Instance.BroadcastMessage(new RemoveMissileMessage(entity), this);
                            }
                        }
                        if (positionX <= 0 || (positionX + Constants.MissileSize) >=
                            (Application.Current.MainWindow.ActualWidth + Game.Instance.Map.Offset))
                        {
                            MessageManager.Instance.BroadcastMessage(new RemoveMissileMessage(entity), this);
                        }
                        break;
                }
            }
        }

        private void collisionWithGround(Entity entity, float deltaT)
        {
            var messageManager = MessageManager.Instance;
            var collision = false;

            var rabbitX = entity.Sprite.Position.X + ((Vector2) entity.Attributes[AttributeEnum.VELOCITY]).X * deltaT;
            var rabbitY = entity.Sprite.Position.Y;

            var rabbitWidth = (int) entity.Attributes[AttributeEnum.WIDTH];
            var rabbitHeight = (int) entity.Attributes[AttributeEnum.HEIGHT];

            if (rabbitX <= 0 || (rabbitX + rabbitWidth) >=
                (Constants.MapWidth * Constants.BlockSize))
            {
                messageManager.BroadcastMessage(new CollisionWithGroundMessage(entity, true, false), this);
            }
            
            if (rabbitX + rabbitWidth >= Game.Instance.Map.endX)
            {
                messageManager.BroadcastMessage(new GameOverMessage(true), this);
                return;
            }

            foreach (var sprite in Game.Instance.Map.GetSprites())
            {
                if (isInCollision(rabbitX, rabbitY, rabbitHeight, rabbitWidth,
                    sprite.Position.X, sprite.Position.Y, Constants.BlockSize,
                    Constants.BlockSize))
                {
                    collision = true;
                    break;
                }
            }

            if (collision)
            {
                messageManager.BroadcastMessage(new CollisionWithGroundMessage(entity, true, false), this);
            }

            collision = false;

            rabbitX = entity.Sprite.Position.X;
            rabbitY = entity.Sprite.Position.Y + ((Vector2) entity.Attributes[AttributeEnum.VELOCITY]).Y * deltaT;
            foreach (var sprite in Game.Instance.Map.GetSprites())
            {
                if (isInCollision(rabbitX, rabbitY, rabbitHeight, rabbitWidth,
                    sprite.Position.X, sprite.Position.Y, Constants.BlockSize,
                    Constants.BlockSize))
                {
                    collision = true;
                    break;
                }
            }

            if (collision)
            {
                messageManager.BroadcastMessage(new CollisionWithGroundMessage(entity, false, true), this);
            }

            collision = false;
            rabbitX = entity.Sprite.Position.X + ((Vector2) entity.Attributes[AttributeEnum.VELOCITY]).X * deltaT;
            rabbitY = entity.Sprite.Position.Y + ((Vector2) entity.Attributes[AttributeEnum.VELOCITY]).Y * deltaT;
            foreach (var sprite in Game.Instance.Map.GetSprites())
            {
                if (isInCollision(rabbitX, rabbitY, rabbitHeight, rabbitWidth,
                    sprite.Position.X, sprite.Position.Y, Constants.BlockSize,
                    Constants.BlockSize))
                {
                    collision = true;
                    break;
                }
            }

            if (collision)
            {
                messageManager.BroadcastMessage(new CollisionWithGroundMessage(entity, true, true), this);
                entity.Attributes[AttributeEnum.ANIMATION_SPEED] = 0f;
            }
            else
            {
                entity.Attributes[AttributeEnum.ANIMATION_SPEED] = Constants.AnimationSpeed;
            }
        }

        private bool isInCollision(float entity1PositionX, float entity1PositionY, int entity1Height, int entity1Width,
            float entity2PositionX, float entity2PositionY, int entity2Height, int entity2Width)
        {
            if ((entity1PositionY + entity1Height) > entity2PositionY &&
                entity1PositionY < (entity2PositionY + entity2Height) &&
                (entity1PositionX + entity1Width) > entity2PositionX &&
                entity1PositionX < (entity2PositionX + entity2Width))
            {
                return true;
            }

            return false;
        }

        public void AddEntity(Entity entity)
        {
            Entities.Add(entity);
        }

        public void RemoveEntity(Entity entity)
        {
            Entities.Remove(entity);
        }
    }
}