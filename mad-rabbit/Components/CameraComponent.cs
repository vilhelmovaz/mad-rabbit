﻿using System.Windows;

namespace mad_rabbit.Components
{
    public class CameraComponent : Component
    {
        public override void OnDestroy()
        {
        }

        public override void OnInit()
        {
        }

        public override void OnMessage(object message, object sender)
        {
        }

        public override void OnUpdate(float deltaT)
        {
            if (Application.Current.MainWindow == null)
            {
                return;
            }

            var thirdWindowWidth = (float) (Application.Current.MainWindow.ActualWidth / 3);
            if ((Game.Instance.InputManager.Right &&
                 Entity.Sprite.Position.X >= thirdWindowWidth + Game.Instance.Map.Offset && Entity.Sprite.Position.X <
                 ((Constants.MapWidth * Constants.BlockSize) - thirdWindowWidth * 2) ||
                 (Game.Instance.InputManager.Left &&
                  Entity.Sprite.Position.X <= thirdWindowWidth + Game.Instance.Map.Offset &&
                  Entity.Sprite.Position.X >= thirdWindowWidth)))
            {
                Game.Instance.Map.Offset = (int)(Entity.Sprite.Position.X - thirdWindowWidth);
            }
        }
    }
}