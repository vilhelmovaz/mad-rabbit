using System.Numerics;

namespace mad_rabbit.Components
{
    public class CloudComponent : Component
    {
        public override void OnInit()
        {
        }

        public override void OnUpdate(float deltaT)
        {
            Entity.Sprite.Position += ((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]) * deltaT;
            var startPosition = (Vector2) Entity.Attributes[AttributeEnum.START_POSITION];
            if (Entity.Sprite.Position.X + (int)Entity.Attributes[AttributeEnum.WIDTH] >= Constants.MapWidth * Constants.BlockSize)
            {
                Entity.Sprite.Position = new Vector2(-(int)Entity.Attributes[AttributeEnum.WIDTH], startPosition.Y);
            }
        }

        public override void OnDestroy()
        {
        }

        public override void OnMessage(object message, object sender)
        {
            
        }
    }
}