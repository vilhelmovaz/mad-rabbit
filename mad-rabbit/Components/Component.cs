﻿using mad_rabbit.Messages;

namespace mad_rabbit.Components
{
    public abstract class Component : IMessageReceiver
    {

        public Entity Entity { get; set; }
        public abstract void OnInit();
        public abstract void OnUpdate(float deltaT);
        public abstract void OnDestroy();
        public abstract void OnMessage(object message, object sender);
        
        public void Subscribe(MessageType type)
        {
            MessageManager.Instance.Subscribe(type, this);
        }

        public void Unsubscribe(MessageType type)
        {
            MessageManager.Instance.Unsubscribe(type, this);
        } 
    }
}