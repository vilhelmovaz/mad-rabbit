﻿using System;
using System.Collections.Generic;
using System.Numerics;
using mad_rabbit.Messages;

namespace mad_rabbit.Components
{
    public class DrawComponent : Component, ISpriteProvider
    {
        private float _animationTime = 0;

        public override void OnInit()
        {
            Game.Instance.DrawingManager.addSpriteProvider(this);
        }

        public override void OnDestroy()
        {
            Game.Instance.DrawingManager.removeFromSpriteProvider(this);
        }

        public override void OnUpdate(float deltaT)
        {
            if (!Entity.Type.Equals(TypeEnum.BIRD) && !Entity.Type.Equals(TypeEnum.SNOOPY) && !Entity.Type.Equals(TypeEnum.RABBIT))
            {
                return;
            }

            if (((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]).Length() > 0)
            {
                _animationTime += deltaT;
            }
            else
            {
                _animationTime = 0;
            }

            var sprite = Entity.Sprite;
            var animationSpeed = (float) Entity.Attributes[AttributeEnum.ANIMATION_SPEED];
            sprite.AnimationFrame = (_animationTime * animationSpeed) % 1f;
        }

        public override void OnMessage(object message, object sender)
        {
            
        }

        public List<Sprite> GetSprites()
        {
            List<Sprite> sprites = new List<Sprite> {Entity.Sprite};
            return sprites;
        }
    }
}