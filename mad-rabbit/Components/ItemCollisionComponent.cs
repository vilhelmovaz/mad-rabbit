using System.Numerics;
using mad_rabbit.Messages;

namespace mad_rabbit.Components
{
    public class ItemCollisionComponent : Component
    {
        public override void OnInit()
        {
            CollisionManager.Instance.AddEntity(Entity);
        }

        public override void OnUpdate(float deltaT)
        {
        }

        public override void OnDestroy()
        {
            CollisionManager.Instance.RemoveEntity(Entity);
        }

        public override void OnMessage(object message, object sender)
        {
        }
    }
}