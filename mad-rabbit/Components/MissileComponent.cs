using System.Numerics;
using mad_rabbit.Messages;

namespace mad_rabbit.Components
{
    public class MissileComponent : Component
    {
        public override void OnInit()
        {
            CollisionManager.Instance.AddEntity(Entity);
        }

        public override void OnUpdate(float deltaT)
        {
            Entity.Sprite.Position += ((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]) * deltaT;
        }

        public override void OnDestroy()
        {
            CollisionManager.Instance.RemoveEntity(Entity);
        }

        public override void OnMessage(object message, object sender)
        {
            
        }
    }
}