using mad_rabbit.Messages;

namespace mad_rabbit.Components
{
    public class MonsterCollisionComponent : Component
    {
        public override void OnInit()
        {
            MessageManager.Instance.Subscribe(MessageType.COLLISION_WITH_MISSILE,this);
            CollisionManager.Instance.AddEntity(Entity);
        }

        public override void OnUpdate(float deltaT)
        {
            
        }

        public override void OnDestroy()
        {
            MessageManager.Instance.Unsubscribe(MessageType.COLLISION_WITH_MISSILE,this);
            CollisionManager.Instance.RemoveEntity(Entity);
        }

        public override void OnMessage(object message, object sender)
        {
            if (message is CollisionWithMissileMessage collisionWithMissile)
            {
                Game.Instance.Score += (float)collisionWithMissile.Monster.Attributes[AttributeEnum.VALUE];
                collisionWithMissile.Monster.Attributes[AttributeEnum.ALIVE] = false;
            }
            
        }
    }
}