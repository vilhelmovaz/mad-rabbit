﻿using System;
using System.Numerics;

namespace mad_rabbit.Components
{
    class MonsterComponent : Component
    {
        public override void OnInit()
        {
        }

        public override void OnUpdate(float deltaT)
        {
            var startPosition = (Vector2) Entity.Attributes[AttributeEnum.START_POSITION];
            var endPosition = (Vector2) Entity.Attributes[AttributeEnum.END_POSITION];
            Entity.Sprite.Position += ((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]) * deltaT;
            if (!Entity.Sprite.Flipped)
            {
               
                if (Entity.Sprite.Position.X >= endPosition.X)
                {
                    Entity.Attributes[AttributeEnum.VELOCITY] = new Vector2(-Constants.Speed, 0);
                    Entity.Sprite.Flipped = true;
                }
            }
            else
            {
                if (Entity.Sprite.Position.X <= startPosition.X)
                {
                    Entity.Attributes[AttributeEnum.VELOCITY] = new Vector2(Constants.Speed, 0);
                    Entity.Sprite.Flipped = false;
                }
            }
        }

        public override void OnDestroy()
        {
        }

        public override void OnMessage(object message, object sender)
        {
            
        }
    }
}