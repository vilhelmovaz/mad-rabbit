using System.Numerics;
using mad_rabbit.Messages;

namespace mad_rabbit.Components
{
    public class PlayerCollisionComponent : Component
    {
        public override void OnInit()
        {
            MessageManager.Instance.Subscribe(MessageType.COLLISION_WITH_GROUND,this);
            MessageManager.Instance.Subscribe(MessageType.COLLISION_WITH_ITEM,this);
            MessageManager.Instance.Subscribe(MessageType.COLLISION_WITH_MONSTER,this);
            CollisionManager.Instance.AddEntity(Entity);
        }

        public override void OnUpdate(float deltaT)
        {
        }

        public override void OnDestroy()
        {
            MessageManager.Instance.Unsubscribe(MessageType.COLLISION_WITH_GROUND,this);
            MessageManager.Instance.Unsubscribe(MessageType.COLLISION_WITH_ITEM,this);
            MessageManager.Instance.Unsubscribe(MessageType.COLLISION_WITH_MONSTER,this);
            CollisionManager.Instance.RemoveEntity(Entity);
        }

        public override void OnMessage(object message, object sender)
        {
            if (message is CollisionWithGroundMessage collisionWithGroundMessage)
            {
                var newX = ((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]).X;
                var newY = ((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]).Y;
                if (collisionWithGroundMessage.X)
                {
                    newX = 0;
                }
                if (collisionWithGroundMessage.Y)
                {
                    Entity.Attributes[AttributeEnum.ON_GROUND] = newY > 0;
                    newY = 0;
                }
                Entity.Attributes[AttributeEnum.VELOCITY] = new Vector2(newX, newY);
            }
            else if (message is CollisionWithItemMessage collisionWithItemMessage)
            {
                Game.Instance.Score += (float) collisionWithItemMessage.Entity.Attributes[AttributeEnum.VALUE];
            }
            else if (message is CollisionWithMonsterMessage collisionWithMonsterMessage)
            {
                collisionWithMonsterMessage.Entity.Sprite.Position = (Vector2)collisionWithMonsterMessage.Entity.Attributes[AttributeEnum.START_POSITION];
                Game.Instance.Lives -= 1;
                collisionWithMonsterMessage.Entity.Attributes[AttributeEnum.ANIMATION_SPEED] = 0f;
                collisionWithMonsterMessage.Entity.Attributes[AttributeEnum.VELOCITY] = new Vector2(0, 0);
                Game.Instance.Map.Offset = 0;
                if (Game.Instance.Lives <= 0)
                {
                    MessageManager.Instance.BroadcastMessage(new GameOverMessage(false), this);
                }
            }
        }
    }
}