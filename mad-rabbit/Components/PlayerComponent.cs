﻿using System.Numerics;
using mad_rabbit.Messages;

namespace mad_rabbit.Components
{
    class PlayerComponent : Component
    {
        public override void OnInit()
        {
            CollisionManager.Instance.AddEntity(Entity);
        }

        public override void OnUpdate(float deltaT)
        {
            var inputManager = Game.Instance.InputManager;

            Entity.Sprite.Position += ((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]) * deltaT;

            var newX = ((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]).X;
            if (inputManager.Right)
            {
                newX = Constants.Speed;
                Entity.Sprite.Flipped = false;
            }
            else if (inputManager.Left)
            {
                newX = -Constants.Speed;
                Entity.Sprite.Flipped = true;
            }
            else
            {
                newX = 0;
            }

            var newY = ((Vector2) Entity.Attributes[AttributeEnum.VELOCITY]).Y;
            if (inputManager.Up && (bool)Entity.Attributes[AttributeEnum.ON_GROUND])
            {
                newY = -Constants.JumpSpeed;
            }
            else
            {
                newY += Constants.Gravity * deltaT;
            }

            Entity.Attributes[AttributeEnum.VELOCITY] = new Vector2(newX, newY);

            Entity.Attributes[AttributeEnum.TIMEOUT] = (float)Entity.Attributes[AttributeEnum.TIMEOUT] - deltaT;
            if (inputManager.Killing && (float)Entity.Attributes[AttributeEnum.TIMEOUT] <= 0)
            {
                var x = Entity.Sprite.Flipped ? Entity.Sprite.Position.X : Entity.Sprite.Position.X + (int) Entity.Attributes[AttributeEnum.WIDTH];
                var y = Entity.Sprite.Position.Y + ((int) Entity.Attributes[AttributeEnum.HEIGHT]/2);
                MessageManager.Instance.BroadcastMessage(new SpawnMissileMessage(
                    new Vector2(x, y), Entity.Sprite.Flipped), this);
                Entity.Attributes[AttributeEnum.TIMEOUT] = Constants.MissileTimeout;
            }
        }

        public override void OnDestroy()
        {
            CollisionManager.Instance.RemoveEntity(Entity);
        }

        public override void OnMessage(object message, object sender)
        {
        }
    }
}