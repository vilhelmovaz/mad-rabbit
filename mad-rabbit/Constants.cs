namespace mad_rabbit
{
    public static class Constants
    {
        public const int BlockSize = 40;
        public const int MapWidth = 120;
        public const int MapHeight = 15;
        public const int PlayerLives = 2;
        public const float RubyValue = 5f;
        public const float DiamondValue = 10f;
        public const float SnoopyValue = 20f;
        public const float BirdValue = 30f;
        public const float AnimationSpeed = 3f;
        public const int PlayerHeight = 65;
        public const int PlayerWidth = 57;
        public const float Speed = 100f;
        public const float JumpSpeed = 410f;
        public const float Gravity = 500f;
        public const int RubySize = 20;
        public const int DiamondSize = 30;
        public const int MissileSize = 7;
        public const float MissileSpeed = 300f;
        public const float MissileTimeout = 0.4f;
        public const int MaxLevel = 5;
        public const float CloudSpeed = 30f;
    }
}