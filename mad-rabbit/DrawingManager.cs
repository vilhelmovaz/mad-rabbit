﻿using System.Collections.Generic;

namespace mad_rabbit
{
    public class DrawingManager
    {
        private List<ISpriteProvider> SpriteProviders = new List<ISpriteProvider>();

        public void addSpriteProvider(ISpriteProvider provider)
        {
            SpriteProviders.Add(provider);
        }

        public void removeFromSpriteProvider(ISpriteProvider provider)
        {
            SpriteProviders.Remove(provider);
        }

        public List<Sprite> GetSpritesForDrawing()
        {
            var sprites = new List<Sprite>();
            foreach (var spriteProvider in SpriteProviders)
            {
                sprites.AddRange(spriteProvider.GetSprites());
            }

            return sprites;
        }
    }
}