﻿using System.Collections.Generic;
using mad_rabbit.Components;

namespace mad_rabbit
{
    public class Entity
    {
        public HashSet<Component> Components { get; set; } = new HashSet<Component>();
        public Sprite Sprite { get; set; } = null;
        public bool Visible { get; set; } = false;
        public TypeEnum Type { get; protected set; }
        public Dictionary<AttributeEnum, object> Attributes { get; set; } = new Dictionary<AttributeEnum, object>();
    }
}