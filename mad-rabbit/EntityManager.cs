﻿using System.Collections.Generic;
using System.Numerics;
using mad_rabbit.Messages;

namespace mad_rabbit
{
    public class EntityManager : IMessageReceiver
    {
        private HashSet<Entity> Entities { get; set; } = new HashSet<Entity>();
        private HashSet<Entity> EntitiesToRemove { get; set; } = new HashSet<Entity>();
        
        private HashSet<Map.EntityDescriptor> MissilesToSpawn { get; set; } = new HashSet<Map.EntityDescriptor>();

        public EntityManager()
        {
            MessageManager.Instance.Subscribe(MessageType.COLLISION_WITH_ITEM,this);
            MessageManager.Instance.Subscribe(MessageType.COLLISION_WITH_MISSILE,this);
            MessageManager.Instance.Subscribe(MessageType.REMOVE_MISSILE,this);
            MessageManager.Instance.Subscribe(MessageType.SPAWN_MISSILE,this);
        }

        public void SpawnEntity(Map.EntityDescriptor entityDescriptor)
        {
            Entity entity = null;
            switch (entityDescriptor.type)
            {
                case TypeEnum.RABBIT:
                    entity = new Player(entityDescriptor.startPosition);
                    break;
                case TypeEnum.SNOOPY:
                    entity = new Monster(TypeEnum.SNOOPY, entityDescriptor.startPosition, entityDescriptor.endPosition);
                    break;
                case TypeEnum.BIRD:
                    entity = new Monster(TypeEnum.BIRD, entityDescriptor.startPosition, entityDescriptor.endPosition);
                    break;
                case TypeEnum.RUBY:
                    entity = new Item(TypeEnum.RUBY);
                    break;
                case TypeEnum.DIAMOND:
                    entity = new Item(TypeEnum.DIAMOND);
                    break;
                case TypeEnum.MISSILE:
                    entity = new Missile(entityDescriptor.flipped);
                    break;
                case TypeEnum.CLOUD1:
                case TypeEnum.CLOUD2:
                case TypeEnum.CLOUD3:
                    entity = new Cloud(entityDescriptor.type, entityDescriptor.startPosition);
                    break;
                default:
                    break;
            }

            if (entity == null)
            {
                return;
            }

            var sprite = new Sprite
            {
                Texture = StringEnum.GetStringValue(entity.Type), Position = entityDescriptor.startPosition, Flipped = entityDescriptor.flipped
            };
            entity.Sprite = sprite;

            foreach (var component in entity.Components)
            {
                component.Entity = entity;
                component.OnInit();
                entity.Components.Add(component);
            }

            Entities.Add(entity);
        }

        public void Update(float deltaT)
        {
            foreach (var entityDescriptor in MissilesToSpawn)
            {
                SpawnEntity(entityDescriptor);
            }
            MissilesToSpawn.Clear();
            foreach (var entity in EntitiesToRemove)
            {
                RemoveEntity(entity);
            }
            EntitiesToRemove.Clear();
            foreach (var entity in Entities)
            {
                if (entity?.Components == null)
                {
                    continue;
                }

                foreach (var comp in entity?.Components)
                {
                    comp.OnUpdate(deltaT);
                }
            }
        }

        private void RemoveEntity(Entity entity)
        {
            foreach (var comp in entity.Components)
            {
                comp.OnDestroy();
            }
            entity.Components.Clear();
            Entities.Remove(entity);
           
        }

        public void ClearEntites()
        {
            var entitiesToRemove = new HashSet<Entity>();
            foreach (var entity in Entities)
            {
                entitiesToRemove.Add(entity);
            }
            foreach (var entity in entitiesToRemove)
            {
                RemoveEntity(entity);
            }
        }

        public void OnMessage(object message, object sender)
        {
            if (message is CollisionWithItemMessage collisionWithItemMessage)
            {
                EntitiesToRemove.Add(collisionWithItemMessage.Entity);
            }
            else if (message is CollisionWithMissileMessage collisionWithMissile)
            {
                EntitiesToRemove.Add(collisionWithMissile.Missile);
                EntitiesToRemove.Add(collisionWithMissile.Monster);
            }

            else if (message is RemoveMissileMessage removeMissileMessage)
            {
                EntitiesToRemove.Add(removeMissileMessage.Missile);
            }
            else if (message is SpawnMissileMessage spawnMissileMessage)
            {
                var entityDescriptor = new Map.EntityDescriptor
                {
                    startPosition = spawnMissileMessage.Position,
                    type = TypeEnum.MISSILE,
                    flipped = spawnMissileMessage.Flipped
                };
                MissilesToSpawn.Add(entityDescriptor);
            }
        }
    }
}