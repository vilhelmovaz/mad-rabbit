﻿using System;
using System.Collections.Generic;
using mad_rabbit.Components;
using mad_rabbit.Messages;

namespace mad_rabbit
{
    public class Game : IMessageReceiver
    {
        private static Game _instance;
        private static readonly object InstanceLock = new object();
        private EntityManager _entityManager = new EntityManager();
        private LevelManager _levelManager = new LevelManager();

        public DrawingManager DrawingManager { get; } = new DrawingManager();
        public InputManager InputManager { get; } = new InputManager();
        public Map Map { get; set; }
        public float Score { get; set; }
        public int Lives { get; set; }
        public int Level { get; set; } = 0;

        public float MaxScore { get; set; }

        public bool NextLevel;
        public bool Gameover { get; set; }
        
        public String Customfile { get; set; }
        

        public static Game Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                lock (InstanceLock)
                {
                    _instance = new Game();
                    MessageManager.Instance.Subscribe(MessageType.GAME_OVER,_instance);
                }

                return _instance;
            }
        }

        public void Init(bool custom)
        {
            if (Level > 0)
            {
                Lives++;
            }
            else
            {
                Lives = Constants.PlayerLives;
            }
            if (custom)
            {
                Level = 100;
            }
            else
            {
                Level++;
            }

            clear();
            _levelManager.LoadLevel(Level);
            foreach (var entityDescriptor in Map.GetEntityDescriptors())
            {
                _entityManager.SpawnEntity(entityDescriptor);
            }
        }

        public void OnUpdate(float deltaT)
        {
            CollisionManager.Instance.Update(deltaT);
            _entityManager.Update(deltaT);
        }

        public void clear()
        {
            Score = 0;
            MaxScore = 0;
            NextLevel = false;
            Gameover = false;
            Map?.clear();
            _entityManager.ClearEntites();
        }

        public void OnMessage(object message, object sender)
        {
            if (message is GameOverMessage gameOverMessage)
            {
                if (gameOverMessage.End)
                {
                    NextLevel = true;
                }
                else
                {
                    Gameover = true;
                }
            }
        }
    }
}