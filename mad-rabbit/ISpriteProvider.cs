﻿using System.Collections.Generic;

namespace mad_rabbit
{
    public interface ISpriteProvider
    {
        List<Sprite> GetSprites();
    }
}