﻿namespace mad_rabbit
{
    public class InputManager
    {
        public bool Up { get; set; }
        public bool Right { get; set; }
        public bool Left { get; set; }
        public bool Killing { get; set; }
    }
}