﻿using mad_rabbit.Components;

namespace mad_rabbit
{
    class Item : Entity
    {
        public Item(TypeEnum type)
        {
            Type = type;
            switch (type)
            {
                case TypeEnum.RUBY:
                    Attributes.Add(AttributeEnum.VALUE, Constants.RubyValue);
                    Attributes.Add(AttributeEnum.SIZE, Constants.RubySize);
                    break;
                case TypeEnum.DIAMOND:
                    Attributes.Add(AttributeEnum.VALUE, Constants.DiamondValue);
                    Attributes.Add(AttributeEnum.SIZE, Constants.DiamondSize);
                    break;
                default:
                    break;
            }
            Components.Add(new ItemCollisionComponent());
            Components.Add(new DrawComponent());
        }
    }
}