﻿namespace mad_rabbit
{
    public class LevelManager
    {
        private const string RESOURCES = "Resources/";

        public void LoadLevel(int level)
        {
            switch (level)
            {
                case 1:
                    Game.Instance.Map = new Map(RESOURCES + "level1.lvl");
                    break;
                case 2:
                    Game.Instance.Map = new Map(RESOURCES + "level2.lvl");
                    break;
                case 3:
                    Game.Instance.Map = new Map(RESOURCES + "level3.lvl");
                    break;
                case 4:
                    Game.Instance.Map = new Map(RESOURCES + "level4.lvl");
                    break;
                case 5:
                    Game.Instance.Map = new Map(RESOURCES + "level5.lvl");
                    break;
                case 100:
                    Game.Instance.Map = new Map(Game.Instance.Customfile);
                    break;
            }
        }
    }
}