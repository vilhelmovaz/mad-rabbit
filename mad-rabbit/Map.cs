﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Windows;

namespace mad_rabbit
{
    public class Map : ISpriteProvider
    {
        private long[,] _data = new long[Constants.MapWidth, Constants.MapHeight];
        private List<Sprite> _sprites = new List<Sprite>();
        private List<EntityDescriptor> _entityDescriptors = new List<EntityDescriptor>();
        public int Offset = 0;
        public float endX;

        public struct EntityDescriptor
        {
            public Vector2 startPosition;
            public Vector2 endPosition;
            public TypeEnum type;
            public int id;
            public bool flipped;
        }

        public Map(string fileName)
        {
            try
            {
                using (var reader = new StreamReader(fileName))
                {
                    var count = 0;
                    while (true)
                    {
                        var line = reader.ReadLine();

                        if (line == null)
                        {
                            break;
                        }

                        var blocks = line.Split(' ');
                        for (var i = 0; i < Constants.MapHeight; i++)
                        {
                            _data[count, i] = Convert.ToInt64(blocks[i]);
                        }

                        count++;
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            for (var i = 0; i < Constants.MapWidth; i++)
            {
                for (var j = 0; j < Constants.MapHeight; j++)
                {
                    var id = 0;
                    var found = false;
                    switch (BlockType.getType(_data[i, j]))
                    {
                        case 1:
                            addSprite(i, j, "grass.png");
                            continue;
                        case 2:
                            addSprite(i, j, "dirt.png");
                            continue;
                        case 3:
                            addSprite(i, j, "stone.png");
                            continue;
                        case 4:
                            Game.Instance.MaxScore += Constants.RubyValue;
                            addEntityDescriptor(i, j, TypeEnum.RUBY, 0);
                            continue;
                        case 5:
                            Game.Instance.MaxScore += Constants.DiamondValue;
                            addEntityDescriptor(i, j, TypeEnum.DIAMOND, 0);
                            continue;
                        case 6:
                            addEntityDescriptor(i, j, TypeEnum.RABBIT, 0);
                            continue;
                        case 7:
                            id = BlockType.getMonsterId(_data[i, j]);
                            found = false;
                            for (var e = 0; e < _entityDescriptors.Count; e++)
                            {
                                var entity = _entityDescriptors[e];
                                var type = entity.type;
                                if (!type.Equals(TypeEnum.SNOOPY))
                                {
                                    continue;
                                }

                                if (entity.id != id)
                                {
                                    continue;
                                }

                                found = true;

                                entity.endPosition = new Vector2(i * Constants.BlockSize, j * Constants.BlockSize);

                                if (entity.endPosition.X < entity.startPosition.X)
                                {
                                    Console.WriteLine("switching");
                                    var tempX = entity.endPosition.X;
                                    entity.endPosition.X = entity.startPosition.X;
                                    entity.startPosition.X = tempX;
                                }

                                _entityDescriptors[e] = entity;
                                break;
                            }

                            if (!found)
                            {
                                addEntityDescriptor(i, j, TypeEnum.SNOOPY, id);
                                Game.Instance.MaxScore += Constants.SnoopyValue;
                            }

                            continue;
                        case 8:
                            id = BlockType.getMonsterId(_data[i, j]);
                            found = false;
                            for (var e = 0; e < _entityDescriptors.Count; e++)
                            {
                                var entity = _entityDescriptors[e];
                                var type = entity.type;
                                if (!type.Equals(TypeEnum.BIRD))
                                {
                                    continue;
                                }

                                if (entity.id != id) continue;
                                found = true;

                                entity.endPosition = new Vector2(i * Constants.BlockSize, j * Constants.BlockSize);

                                if (entity.endPosition.X < entity.startPosition.X)
                                {
                                    Console.WriteLine("switching");
                                    var tempX = entity.endPosition.X;
                                    entity.endPosition.X = entity.startPosition.X;
                                    entity.startPosition.X = tempX;
                                }

                                _entityDescriptors[e] = entity;
                                break;
                            }

                            if (!found)
                            {
                                addEntityDescriptor(i, j, TypeEnum.BIRD, id);
                                Game.Instance.MaxScore += Constants.BirdValue;
                            }

                            continue;
                    }
                    if (BlockType.isEnd(_data[i, j]))
                    {
                        endX = i * Constants.BlockSize;
                    }
                }
            }
            Random rnd = new Random(DateTime.Now.Millisecond);
            var clouds = new TypeEnum[] {TypeEnum.CLOUD1, TypeEnum.CLOUD2, TypeEnum.CLOUD3};
            for (var i = 0; i < 15; i++)
            {
                addEntityDescriptor(rnd.Next(0, Constants.MapWidth ), rnd.Next(0, Constants.MapHeight/3) , clouds[rnd.Next(0,2)], 0);
            }
            Game.Instance.DrawingManager.addSpriteProvider(this);
        }

        private void addSprite(int positionX, int positionY, string textureName)
        {
            var sprite = new Sprite
            {
                Position =
                    new Vector2(positionX * Constants.BlockSize, positionY * Constants.BlockSize),
                Texture = textureName
                
            };
            _sprites.Add(sprite);
        }

        private void addEntityDescriptor(int startPositionX, int startPositionY, TypeEnum type, int id)
        {
            var entityDescriptor = new EntityDescriptor
            {
                startPosition =
                    new Vector2(startPositionX * Constants.BlockSize, startPositionY * Constants.BlockSize),
                type = type,
                id = id
            };
            _entityDescriptors.Add(entityDescriptor);
        }

        public void clear()
        {
            _entityDescriptors.Clear();
            _sprites.Clear();
            Offset = 0;
            Game.Instance.DrawingManager.removeFromSpriteProvider(this);
        }


        public List<Sprite> GetSprites()
        {
            return _sprites;
        }

        public List<EntityDescriptor> GetEntityDescriptors()
        {
            return _entityDescriptors;
        }
    }
}