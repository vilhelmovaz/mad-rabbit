namespace mad_rabbit.Messages
{
    public class CollisionWithGroundMessage : Message
    {
        private Entity Entity { get; }
        public bool X { get; }
        public bool Y { get; }

        public CollisionWithGroundMessage(Entity entity, bool x, bool y)
        {
            Entity = entity;
            Y = y;
            X = x;
            Type = MessageType.COLLISION_WITH_GROUND;
        }
    }
}