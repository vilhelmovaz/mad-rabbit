namespace mad_rabbit.Messages
{
    public class CollisionWithItemMessage : Message
    {
        public Entity Entity { get; }
        
        public CollisionWithItemMessage(Entity entity)
        {
            Entity = entity;
            Type = MessageType.COLLISION_WITH_ITEM;
        }
    }
}