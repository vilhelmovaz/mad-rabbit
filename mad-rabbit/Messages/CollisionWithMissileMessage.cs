namespace mad_rabbit.Messages
{
    public class CollisionWithMissileMessage : Message
    {
        public Entity Monster { get; }
        public Entity Missile { get; }
        
        public CollisionWithMissileMessage(Entity monster, Entity missile)
        {
            Monster = monster;
            Missile = missile;
            Type = MessageType.COLLISION_WITH_MISSILE;
        }
    }
}