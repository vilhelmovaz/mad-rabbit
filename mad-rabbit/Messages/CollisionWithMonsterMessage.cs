namespace mad_rabbit.Messages
{
    public class CollisionWithMonsterMessage : Message
    {
        public Entity Entity { get; }
        
        public CollisionWithMonsterMessage(Entity entity)
        {
            Entity = entity;
            Type = MessageType.COLLISION_WITH_MONSTER;
        }
    }
}