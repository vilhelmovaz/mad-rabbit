namespace mad_rabbit.Messages
{
    public class GameOverMessage : Message
    {
        public bool End { get; }
        
        public GameOverMessage(bool end)
        {
            End = end;
            Type = MessageType.GAME_OVER;
        }
    }
}