﻿namespace mad_rabbit.Messages
{
    public interface IMessageReceiver
    {
        void OnMessage(object message, object sender);
    }
}