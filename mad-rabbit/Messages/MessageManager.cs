﻿using System;
using System.Collections.Generic;

namespace mad_rabbit.Messages
{
    public class MessageManager
    {
        private  Dictionary<MessageType, List<IMessageReceiver>> receivers = new Dictionary<MessageType, List<IMessageReceiver>>();

        private static MessageManager _instance;
        private static readonly object InstanceLock = new object();

        public static MessageManager Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                lock (InstanceLock)
                {
                    _instance = new MessageManager();
                }

                return _instance;
            }
        }

        public void Subscribe(MessageType type, IMessageReceiver messageReceiver)
        {
            var list = receivers.ContainsKey(type) ? receivers[type] : null;
            receivers[type] = list == null ? (list = new List<IMessageReceiver>()) : list;
            list.Add(messageReceiver);
        }

        public void Unsubscribe(MessageType type, IMessageReceiver messageReceiver)
        {
            receivers[type].Remove(messageReceiver);
        }

        public void BroadcastMessage(Message message, object sender)
        {
            foreach (var receiver in receivers[message.Type])
            {
                receiver.OnMessage(message, sender);
            }
        }
    }
}