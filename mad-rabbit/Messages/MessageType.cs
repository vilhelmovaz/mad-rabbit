namespace mad_rabbit.Messages
{
    public enum MessageType
    {
        COLLISION_WITH_GROUND,
        COLLISION_WITH_ITEM,
        COLLISION_WITH_MISSILE,
        COLLISION_WITH_MONSTER,
        GAME_OVER,
        REMOVE_MISSILE,
        SPAWN_MISSILE
    }
}