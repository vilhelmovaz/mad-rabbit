namespace mad_rabbit.Messages
{
    public class RemoveMissileMessage : Message
    {
        public Entity Missile { get; }
        
        public RemoveMissileMessage(Entity missile)
        {
            Missile = missile;
            Type = MessageType.REMOVE_MISSILE;
        }
    }
}