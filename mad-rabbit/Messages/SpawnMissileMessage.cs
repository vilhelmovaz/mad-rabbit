using System;
using System.Numerics;

namespace mad_rabbit.Messages
{
    public class SpawnMissileMessage : Message
    {    
        public Vector2 Position { get; }
        public bool Flipped { get; }
        
        public SpawnMissileMessage(Vector2 position, bool flipped)
        {
            Position = position;
            Flipped = flipped;
            Type = MessageType.SPAWN_MISSILE;
        }
    }
}