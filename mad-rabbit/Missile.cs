using System.Numerics;
using System.Windows;
using mad_rabbit.Components;

namespace mad_rabbit
{
    public class Missile : Entity
    {
        public Missile(bool flipped)
        {
            Type = TypeEnum.MISSILE;
            Attributes.Add(AttributeEnum.VELOCITY, new Vector2(flipped ? -Constants.MissileSpeed : Constants.MissileSpeed, 0));
            Components.Add(new DrawComponent());
            Components.Add(new MissileComponent());
            Components.Add(new ItemCollisionComponent());
        }
    }
}