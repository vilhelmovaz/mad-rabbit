﻿using System.Numerics;
using mad_rabbit.Components;

namespace mad_rabbit
{
    public class Monster : Entity
    {
        public Monster(TypeEnum type, Vector2 startPosition, Vector2 endPosition)
        {
            Attributes.Add(AttributeEnum.START_POSITION, startPosition);
            Attributes.Add(AttributeEnum.END_POSITION, endPosition);
            Attributes.Add(AttributeEnum.VELOCITY, new Vector2(Constants.Speed, 0));
            Attributes.Add(AttributeEnum.ANIMATION_SPEED, Constants.AnimationSpeed);
            Attributes.Add(AttributeEnum.ALIVE, true);
            Attributes.Add(AttributeEnum.VALUE, Type.Equals(Type == TypeEnum.SNOOPY) ? Constants.SnoopyValue : Constants.BirdValue);
            Type = type;
            Components.Add(new DrawComponent());
            Components.Add(new MonsterComponent());
            Components.Add(new MonsterCollisionComponent());
        }
    }
}