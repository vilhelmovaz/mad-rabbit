﻿using System.Numerics;
using mad_rabbit.Components;

namespace mad_rabbit
{
    class Player : Entity
    {
        public Player(Vector2 startPosition)
        {
            Type = TypeEnum.RABBIT;
            Attributes.Add(AttributeEnum.START_POSITION, startPosition);
            Attributes.Add(AttributeEnum.VELOCITY, new Vector2(0, 0));
            Attributes.Add(AttributeEnum.ANIMATION_SPEED, 0f);
            Attributes.Add(AttributeEnum.HEIGHT, Constants.PlayerHeight);
            Attributes.Add(AttributeEnum.WIDTH, Constants.PlayerWidth);
            Attributes.Add(AttributeEnum.ON_GROUND, false);
            Attributes.Add(AttributeEnum.TIMEOUT, 0f);
            Components.Add(new DrawComponent());
            Components.Add(new PlayerComponent());
            Components.Add(new CameraComponent());
            Components.Add(new PlayerCollisionComponent());
        }
    }
}