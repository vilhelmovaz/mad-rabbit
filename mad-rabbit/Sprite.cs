﻿using System.Numerics;

namespace mad_rabbit
{
    public class Sprite
    {
        //! Centered position
        public Vector2 Position { get; set; }
        public string Texture { get; set; }
        public bool Flipped { get; set; }
        public float AnimationFrame { get; set; } = 0;
    }
}