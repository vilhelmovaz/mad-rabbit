﻿using System;

namespace mad_rabbit
{
    public static class StringEnum
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;
            var type = value.GetType();

            //Check first in our cached results...

            //Look for our 'StringValueAttribute' 

            //in the field's custom attributes

            var fi = type.GetField(value.ToString());
            if (fi.GetCustomAttributes(typeof(StringValue),
                    false) is StringValue[] attrs && attrs.Length > 0)
            {
                output = attrs[0].Value;
            }

            return output;
        }
    }
}