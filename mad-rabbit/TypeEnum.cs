﻿namespace mad_rabbit
{
    public enum TypeEnum
    {
        [StringValue("bird.3.png")] BIRD = 0,
        [StringValue("rabbit.5.png")] RABBIT = 1,
        [StringValue("snoopy.2.png")] SNOOPY = 2,
        [StringValue("ruby.png")] RUBY = 3,
        [StringValue("diamond.png")] DIAMOND = 4,
        [StringValue("missile.png")] MISSILE = 5,
        [StringValue("cloud1.png")] CLOUD1 = 6,
        [StringValue("cloud2.png")] CLOUD2 = 7,
        [StringValue("cloud3.png")] CLOUD3 = 8
    }
}